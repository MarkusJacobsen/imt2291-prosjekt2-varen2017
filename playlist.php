<?php
session_start();
require_once('include\db.php');
require_once('classes\user.php');


if(isset($_POST['action']) && !empty($_POST['action'])){
  $description = "";
  $type = false;

  $action = $_POST['action'];


  if(isset($_POST['type']) && $_POST['type'] == "dropdownPlaylist"){
    $type = true;
  }
  if(isset($_POST['title'])){
	$title = $_POST['title'];
	}
  if(isset($_POST['Ntitle'])){
  $Ntitle = $_POST['Ntitle'];
  }
  if(isset($_POST['description'])){
	$description = $_POST['description'];
	}
  if(isset($_POST['Ndescription'])){
	$Ndescription = $_POST['Ndescription'];
	}
  if(isset($_POST['userId'])){
	$uid = $_POST['userId'];
	}
  if(isset($_POST['pid'])){
	$pid = $_POST['pid'];
	}
  if(isset($_POST['vid'])){
	$vid = $_POST['vid'];
	}


  switch($action){
		case 'addPlaylist': addPlaylist($db, $uid, $title, $description); break;
    case 'listPlaylists': listPlaylists($db, $uid, $type); break;
    case 'userPlaylist': userPlaylist($db, $title); break;
    case 'updatePlaylist': updatePlaylist($db, $pid, $Ntitle, $Ndescription); break;
    case 'deletePlaylistVideo': deletePlaylistVideo($db, $pid, $vid); break;
    case 'addToMyPlaylist': addToMyPlaylist($db, $pid, $vid); break;
		default: die("Access denied for this function");
	}
}

function addPlaylist($db, $uid, $title, $description){
  $sql = "INSERT INTO playlist(owner, title, description) VALUES (?, ?, ?)";
  $sth = $db->prepare($sql);
  $sth->execute(array($uid, $title, $description));
}

function listPlaylists($db, $uid, $type){
  $sql="SELECT * FROM playlist WHERE owner=?";
  $sth = $db->prepare ($sql);
  $sth->execute (array($uid));

  while($row=$sth->fetch(PDO::FETCH_ASSOC)) {
    $PID = $row['pid'];
    if($type){
      echo "<li><a href=\"#\" id=\"dropdownPl$PID\">";
      echo $row['title'] . "</a></li>";
    } else {
      echo "<li><a href=\"#\" id=\"playlist$PID\">";
      echo $row['title'] . "</a></li>";
    }
  }
}

function userPlaylist($db, $title){
  $sql="SELECT * FROM playlist WHERE title = ?";
  $sth = $db->prepare ($sql);
  $sth->execute (array($title));

  while($row=$sth->fetch(PDO::FETCH_ASSOC)) {
    $pid = $row["pid"];
    $Ptitle = $row["title"];
    $description = $row["description"];
  }
  echo "<div id=\"playlistContainer\" class=\"container\">
    <div class=\"row\">
      <div class=\"col-xs-6 col-lg-12\">
       <div class=\"panel panel-default\" style=\"margin-left: 100px;\">
        <div class=\"panel-heading clearfix\">
          <h3 id=\"$pid\" class=\"panel-title pull-left\">$Ptitle</h3>&nbsp;
          <button class=\"panel-title pull-right\">
            <a id=\"redigerSpilleliste\">Rediger</a></button>
        </div>
          <div class=\"panel-body\">
            <h5><b>Beskrivelse</b><br>$description</h5>
            <table id=\"playlistTable\"  class=\"table table-striped\">
              <thread>
                <th></th><th style=\"width:150px\">Tittel</th><th style=\"width:50px\">Beskrivelse</th><th>Publisert</th><th></th>
              </thread>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>";
}

function updatePlaylist($db, $pid, $Ntitle, $Ndescription){
  $sql = "UPDATE playlist SET title= ?, description= ? WHERE pid = ?";
  $sth = $db->prepare ($sql);
  $sth->execute (array($Ntitle, $Ndescription, $pid));
}

function deletePlaylistVideo($db, $pid, $vid){
  $sql = 'DELETE FROM playlistentry WHERE pid=? AND vid=?';
  $sth = $db->prepare ($sql);
  $sth->execute (array ($pid, $vid));
}

function addToMyPlaylist($db, $pid, $vid){
  $sql = "INSERT INTO playlistentry(pid, vid) VALUES (?, ?)";
  $sth = $db->prepare($sql);
  $sth->execute(array($pid, $vid));
}

?>
