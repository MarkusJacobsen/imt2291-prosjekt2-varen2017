<?php
session_start();
require_once('include\db.php');

if(isset($_POST['action']) && !empty($_POST['action'])){
  $action = $_POST['action'];
  if(isset($_POST['Nemail'])){
	$email = $_POST['Nemail'];
	}
  if(isset($_POST['email'])){
	$email = $_POST['email'];
	}
  if(isset($_POST['Npassword'])){
	$password = password_hash($_POST['Npassword'], PASSWORD_DEFAULT);
	}
  if(isset($_POST['Naccess_level'])){
	$access_level = ($_POST['Naccess_level']?'admin':'user');
	}
  switch($action){
		case 'addUser': addUser($db, $email, $password, $access_level); break;
    case 'listUsers': listUsers($db); break;
    case 'deleteUser': deleteUser($db, $email); break;
		default: die("Access denied for this function");
	}
}

function addUser($db, $email, $password, $access_level) {
  $sql = "INSERT INTO users(email, password, access_level) VALUES (?, ?, ?)";
  $sth = $db->prepare($sql);
  $sth->execute(array($email, $password, $access_level));
  echo $email;
}

function listUsers($db){
  $sql="SELECT * FROM users";
  $sth = $db->prepare ($sql);
  $sth->execute ();

  while($row=$sth->fetch(PDO::FETCH_ASSOC)) {
    echo "<tr>";
    echo "<td>" . $row['email'] . "</td>" . "<td>" . $row['access_level'] . "</td><td><a id='deleteUser' ><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a></td>";
    echo "</tr>";
  }
}

function deleteUser($db, $email){
  $sql = 'DELETE FROM users WHERE email=?';
    $sth = $db->prepare ($sql);
    $sth->execute (array ($email));
}

?>
