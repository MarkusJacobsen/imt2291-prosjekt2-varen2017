<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="signin.css"/>
    <title><?php echo $_GET['list']?></title>
</head>
<body>
  <?php
session_start();
require_once('include/db.php');
require_once('include/heading.php');
require_once('include/playlistStream.php');
require_once('classes/playlist.php');

$sql = "SELECT * FROM playlist WHERE title =?";
$sth = $db->prepare($sql);
$sth->execute(array($_GET['list']));
while($row = $sth->fetch(PDO::FETCH_ASSOC)){
  $pid = $row['pid'];
  $Ptitle = $row['title'];
  $description = $row['description'];
}

if(isset($_GET['del'])){
  $res = $playlist->deleteFromPlaylist($_GET['del']);
  if (isset($res['success'])) {?>
    <div class="alert alert-success alert-dismissible" align="center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Video slettet
    </div><?php
    unset($_GET['del']);
  } else {?>
    <div class="alert alert-danger" role="alert">Kunne ikke slette video</div><?php
  }
}

if(isset($_POST['Ntitle'])&&isset($_POST['Ndescription'])) {
  $res = $playlist->updatePlaylist($pid, $_POST['Ntitle'], $_POST['Ndescription']);
  $newURL = $_SERVER['PHP_SELF'] . '?list=' . $_POST['Ntitle'];
  if (isset($res['success'])) {
    //echo "<a href='{$_SERVER['PHP_SELF']}?list={$_POST['Ntitle']}'></a>";?>
    <script type="text/javascript">
      window.location.replace("<?php echo $newURL ?>");
      $(function() {	// When document is loaded, hide the login and show signup
        $('#editBox').hide();

      });
    </script><?php
  } else {?>
    <div class="alert alert-danger" role="alert" align="center">Kunne ikke oppdatere</div><?php
  }
}
   ?>

   <div class="container">
     <div class="row">
       <div class="col-xs-6 col-lg-12">
        <div class="panel panel-default" style="margin-left: 100px;">
         <div class="panel-heading clearfix">
   		     <h3 class="panel-title pull-left"><?php echo $Ptitle?></h3>&nbsp;
           <button class="panel-title pull-right">
             <a href="<?php echo $_SERVER['PHP_SELF'].'?list='.$Ptitle.'&edit=true'?>">Rediger</a></button>
   		   </div>
   	       <div class="panel-body">
             <h5><b>Beskrivelse</b><br><?php echo $description?></h5>
             <table id="playlistTable"  class="table table-striped">
               <thread>
                 <th style="width:200px">Tittel</th><th style="width:50px">Beskrivelse</th><th></th><th></th>
               </thread>
               <tbody><?php
                 $sql = "SELECT vid FROM `playlistentry` WHERE pid=?";
                 $sth = $db->prepare($sql);
                 $sth->execute(array($pid));
                 while($row=$sth->fetch(PDO::FETCH_ASSOC)){
                   $vid = $row['vid'];
                   $sql2 = "SELECT uploader, title, description FROM video WHERE id=?";
                   $sth2 = $db->prepare($sql2);
                   $sth2->execute(array($vid));

                   $sql3 = "SELECT thumbnail_filepath FROM videoextra WHERE vid=?";
                   $sth3 = $db->prepare($sql3);
                   $sth3->execute(array($vid));
                   while($row3=$sth3->fetch(PDO::FETCH_ASSOC)){
                     $thumb_src = $row3['thumbnail_filepath'];
                   }
                   while($row2=$sth2->fetch(PDO::FETCH_ASSOC)){
                     echo '<tr>';
                      echo "<td><img src='$thumb_src' class='img-thumbnail' alt='thumb' width='120' height='90'></td>";
                     echo "<td><a href='displayVideo.php?id=$vid'>{$row2['title']}</a></td><td>{$row2['description']}</a></td>";
                     echo "<td><a href='{$_SERVER['PHP_SELF']}?list=$Ptitle&del="."{$row['vid']}'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span>Slett fra listen</a></td>";
                     echo '</tr>';
                   }
                 }?>
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div id="editBox" class="col-xs-6 col-lg-12">
         <div class="panel panel-default" style="margin-left: 100px">
           <div class="panel-heading">
             <div class="panel-title">Rediger</div>
           </div>
             <div class="panel-body" style="margin-top: 10px; margin-left: 20px;">
               <form method="post" action="<?php echo $_SERVER['PHP_SELF'].'?list='.$Ptitle.'&edit=true'?>" role="form">
                 <div class="row">
                   <div class="row-xs-10">
                     <div style="margin-bottom: 25px" class="input-group">Tittel
                       <input type="text" class="form-control" name="Ntitle" required placeholder="Tittel">
                     </div>
                   </div>
                   <div class="row-xs-10">
                     <div style="margin-bottom: 25px" class="input-group">Beskrivelse
                       <textarea class="form-control" name="Ndescription" placeholder="Beskrivelse"></textarea>
                     </div>
                   </div>
                   <div style="margin-top:10px" class="form-group">
                     <!-- Button -->

                     <div class="col-sm-12 controls">
                       <input type="submit" id="btn-login" class="btn btn-success" value="Submit"/>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
   <script>
    $(function() {	// When document is loaded, hide the login and show signup
     $('#editBox').hide();
    });
   </script>
   <?php
   if (isset ($_GET['edit'])) { ?>
   	<script type="text/javascript">
   		$(function() {	// When document is loaded, hide the login and show signup
        window.scrollTo(0,document.body.scrollHeight);
   			$('#editBox').show();
   		});<?php
    } ?>
   	</script>
 </body>
 </html>
