<?php
session_start();
require_once 'include/db.php';
$counter = 0;

$type = false;
if(isset($_POST['type']) && $_POST['type'] == "dropdownPlaylist"){
  $type = true;
}

$sql="SELECT * FROM playlist";
$sth = $db->prepare ($sql);
$sth->execute ();

while($row=$sth->fetch(PDO::FETCH_ASSOC)) {
  $PID = $row['pid'];
  if($type){
    echo "<li><a href=\"#\" id=\"dropdownPl$PID\">";
    echo $row['title'] . "</a></li>";
  } else {
    echo "<li><a href=\"#\" id=\"playlist$PID\">";
    echo $row['title'] . "</a></li>";
  }
}
?>
