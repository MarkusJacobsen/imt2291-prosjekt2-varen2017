<!DOCTYPE html>
<html>
  <head>

    <title>Mine videoer</title>
</head>
<body>
 
    <div class="alert alert-success alert-dismissible alert-xs" align="center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Video slettet
    </div>
    <div class="row">
      <div class="alert alert-danger" align="center" role="alert">Kunne ikke slette video</div>
    </div>
 
    <script type="text/javascript">
      $(function() {	// When document is loaded, hide the login and show signup
        $('#editBox').hide();
      });
    </script>
    <div class="alert alert-danger" align="center" role="alert">Kunne ikke oppdatere</div>

<div class="container">
  <div class="row">
    <div class="col-xs-6 col-lg-12">
      <div class="panel panel-default" style="margin-left: 100px;">
        <div class="panel-heading clearfix">
          <div class="panel-title">Mine videoer</div>
        </div>
        <div class="panel-body">
          <table id="myVideoTable"  class="table table-striped">
            <thread>
              <th></th><th style="width:200px">Tittel</th><th style="width:50px">Beskrivelse</th><th></th><th></th><th></th>
            </thread>
            <tbody>
             
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div id="editBox" class="col-xs-6 col-lg-12">
      <div class="panel panel-default" style="margin-left: 100px;">
        <div class="panel-heading">
          <div class="panel-title">Rediger</div>
        </div>
        <div class="panel-body" style="margin-top: 10px; margin-left: 20px;">
          <form method="post" action="<?php echo $_SERVER['PHP_SELF'].'?vid='.$_GET['vid']?>" role="form">
            <div class="row">
              <div class="row-xs-10">
                <div style="margin-bottom: 25px" class="input-group">Tittel
                  <input type="text" class="form-control" name="Ntitle" required placeholder="Tittel">
                </div>
              </div>
              <div class="row-xs-10">
                <div style="margin-bottom: 25px" class="input-group">Beskrivelse
                  <textarea class="form-control" name="Ndescription" placeholder="Beskrivelse"></textarea>
                </div>
                <div class="row-xs-10">
                  <div style="margin-bottom: 25px" class="input-group">New tags
                    <textarea class="form-control" name="Ntags" placeholder="Tags, separated by space"></textarea>
                  </div>
              </div>
              <div style="margin-top:10px" class="form-group">
                <!-- Button -->

                <div class="col-sm-12 controls">
                  <input type="submit" id="btn-login" class="btn btn-success" value="Submit"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
  $(function() {	// When document is loaded, hide the login and show signup
    $('#editBox').hide();
  });
  </script>
  <?php
  if (isset ($_GET['edit'])) { ?>
    <script type="text/javascript">
    $(function() {	// When document is loaded, hide the login and show signup
      $('#editBox').show();
       window.scrollTo(0,document.body.scrollHeight);
    });<?php
  } ?>
  </script>
</body>
</html>
