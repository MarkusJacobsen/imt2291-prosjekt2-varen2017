<?php


class Playlist {
  var $db;

  function Playlist($db){
    $this->db = $db;
  }


  function newPlayList ($owner, $name, $description) {
			$sql = "INSERT INTO playlist (owner, title, description) VALUES (?, ?, ?)";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($owner, $name, $description));
      $pid = $this->db->lastInsertId();
      if ($sth->rowCount()==0) {
        return (array ('error'=>'error', 'description'=>'Something went wrong'));
      }
      return (array ('success'=>'success'));
	}

  function addEntryToPlaylist($vid, $playlistName) {
    $sql = "SELECT pid FROM playlist WHERE title=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($playlistName));
    while($row = $sth->fetch(PDO::FETCH_ASSOC)){
      $pid = $row['pid'];
    }
    $sql2 = "INSERT INTO playlistentry(pid, vid) VALUES (?, ?)";
    $sth2 = $this->db->prepare($sql2);
    $sth2->execute(array($pid, $vid));
    if ($sth2->rowCount()==0) {
      return (array ('error'=>'error', 'description'=>'Something went wrong'));
    }
    return (array ('success'=>'success'));
  }


  function deleteFromPlaylist($vid){
    $sql = "DELETE FROM playlistentry WHERE vid=?";
    $sth = $this->db->prepare ($sql);
    $sth->execute (array ($vid));
    if($sth->rowCount()==0){
      return (array ('error'=>'Something went wrong'));
    }
    return (array ('success'=>'success'));
  }

  function updatePlaylist($id, $title, $description) {
    $sql = "UPDATE playlist SET title= ?, description= ? WHERE pid = ?";
    $sth = $this->db->prepare ($sql);
    $sth->execute (array($title, $description, $id));
    if($sth->rowCount()==0){
      return (array('error'=>'Something went wrong'));
    }
    return array('success'=>'success');
  }


}

$playlist = new Playlist($db);
