<?php
session_start();
require_once "include/db.php";

$sql="SELECT * FROM playlist WHERE title = ?";
$sth = $db->prepare ($sql);
$sth->execute (array($_POST['title']));

while($row=$sth->fetch(PDO::FETCH_ASSOC)) {
  $pid = $row["pid"];
  $Ptitle = $row["title"];
  $description = $row["description"];
}
echo "<div id=\"playlistContainer\" class=\"container\">
  <div class=\"row\">
    <div class=\"col-xs-6 col-lg-12\">
     <div class=\"panel panel-default\" style=\"margin-left: 100px;\">
      <div class=\"panel-heading clearfix\">
        <h3 id=\"$pid\" class=\"panel-title pull-left\">$Ptitle</h3>&nbsp;
        <button class=\"panel-title pull-right\">
          <a id=\"redigerSpilleliste\">Rediger</a></button>
      </div>
        <div class=\"panel-body\">
          <h5><b>Beskrivelse</b><br>$description</h5>
          <table id=\"playlistTable\"  class=\"table table-striped\">
            <thread>
              <th></th><th style=\"width:150px\">Tittel</th><th style=\"width:50px\">Beskrivelse</th><th>Publisert</th><th></th>
            </thread>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>";

?>
