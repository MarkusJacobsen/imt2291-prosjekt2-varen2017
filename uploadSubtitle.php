<?php
session_start();
require_once('include\db.php');
require_once('classes\user.php');
require_once ('include\heading.php');

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Last opp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>

</head>
<body>

<?php
/*$sql = "SELECT * FROM video WHERE id =?";
$sth = $db->prepare($sql);
$sth->execute(array($_GET['id']));
while($row = $sth->fetch(PDO::FETCH_ASSOC)){
  $id = $row['id'];
}*/
$id = $_GET['id'];



if(isset($_POST['title'])){
	$language = $_POST['language'];

	$uploadOk = 1;
	$file = $_FILES['file'];
  $name = $file['name'];

  //$target_dir = '/xampp/htdocs/imt2291-prosjekt2-varen2017/uploads/subtitle/';
  $target_dir = 'uploads/subtitle/';

  if(empty($errors)==true)
  {
			if(is_dir($target_dir)==false){
      mkdir("$target_dir", 755);     // Create directory if it does not exist
    }

			$path = $target_dir . basename($name);
			if(move_uploaded_file($_FILES['file']['tmp_name'], $path)){
				echo '';
			} else {
				echo "Something went wrong";
			}



		$url ="http://localhost/imt2291-prosjekt2-varen2017/uploads/subtitle/$name";

		$sql = "INSERT INTO subtitles(id, filename, language, filepath) VALUES (?, ?, ?, ?)";
		$stmt = $db->prepare($sql);


		$stmt->execute(array($id, $_POST['title'], $language, $url));
    ?>
    <script type="text/javascript">
      window.location.replace("displayVideo.php?id=<?php echo $id?>");
    </script><?php
		}

	}



?>
<div class = "container">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="panel-title">Last opp Subtitles</div>
    </div>
    <div style="padding-top:30px" class="panel-body">

     <!-- <form method="get" action="uploadSubtitle.php" enctype="multipart/form-data" onsubmit="checkboxCheck();"> -->

      <form method="post" action="<?php echo $_SERVER['PHP_SELF'].'?id='.$_GET['id']?>" enctype="multipart/form-data" onsubmit="checkboxCheck();">

        <div class="form-group">
          <label for="title">Subtitle tittel</label>
          <input type="text" class="form-control" name="title" required id="title" placeholder="Tittel">
        </div>
        <style>
        #tab{ display:inline-block;
          margin-left: 40px; }
          #tab2{
            margin-left: 10px; }
            </style>
            <br/>English<span id='tab2'><input type="checkbox" name="language" value="en" /><br /><br/>
              German<span id='tab2'> <input type="checkbox" name="language" value="ger" /><br/><br/>
                Spanish <span id='tab2'><input type="checkbox" name="language" value="esp" /><br/><br />
                  Norwegian<span id='tab2'> <input type="checkbox" name="language" value="no" /><br/><br />
                    <input type="hidden" id="filename" name="filename"/>
                    <input type="hidden" id="filepath" name="filepath"/>
                    <div class="form-group">
                      <input type="file" name="file" class="upload" />
                      <input class="btn btn-primary center-block" id="submitbutton" type="submit"/>
                    </div>
                  </div>
                  </form>
                </div>
              </div>
              <script>

              function checkboxCheck() {
                var checkboxs=document.getElementsByName("language");
                var okay=false;
                var c = 0;
                var l = checkboxs.length
                for(var i=0; i < l;i++) {
                  if(checkboxs[i].checked) {
                    okay=true;
                    c = c + 1;
                  }
                }
                if (!okay) {
                  alert("Please check a checkbox");
                  event.preventDefault();
                }
                if(c != 1){
                  okay=false;
                  alert("Don't check more than one box");
                  event.preventDefault();
                }
              }
              </script>
            </body>
