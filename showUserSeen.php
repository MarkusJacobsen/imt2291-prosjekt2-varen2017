<?php

session_start();
header("Content-Type: application/json");
require_once('include\db.php');
require_once('classes\user.php');


// Henter video tittel og email for brukere som er blitt logget.
$sql_2 = "SELECT video.title, show_user.email FROM show_user
INNER JOIN video ON show_user.videoid = video.id";
$res2 = $db->query($sql_2);
echo json_encode($res2->fetchAll(PDO::FETCH_ASSOC));
