<?php
/**
*!!Skriv beskrivelse av klassen her senere!!
*
*
* @author groupSuper based mostly on Øyvind Kolloens scripts for users from
* bookmarks_week3_solution
*/

class user {
  var $db;
  var $uid = -1;
  var $email = null;
  var $admin = false;
  var $unknownUser = null;
  var $alert = null;

/**
*!!Skriv logikk i user konstruktor her senere!!
*/

  function User ($db) {
    $this->db = $db;

    //print_r($_POST);
/*--------------------------------------------------------------------------------------------------*/
    if(isset($_GET['logout'])){
			
      unset($_SESSION['uid']);
      if(isset($_COOKIE['persistent']))
        $this->removePersistentLogin();
    }
		
/*--------------------------------------------------------------------------------------------------*/		
		else if (isset($_POST['login-email'])) {	// Logging in
		
			$sql = "SELECT id, password, access_level FROM users WHERE email=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_POST['login-email']));	// Get user info from the database
			if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// If user exists
				if (password_verify ($_POST['login-password'], $row['password'])) {		// If correct password
				
					$_SESSION['uid'] = $row['id'];		// Store user id in session variable
					$this->uid = $row['id'];			// Store user id in object
          $this->email = $row['login-email'];
          $this->admin = ($row['access_level'] == 'admin'?1:0);
					
					if (isset($_POST['remember'])&&$_POST['remember']=='1')		  // Logging in and remember login							
						$this->persistentLogin();
				} 
				else {	// Bad password
					$this->unknownUser = 'Uknown username/password';
					// Note, never say bad password, then you confirm the user exists
				}
			} 
			else {		// Unknow user
				$this->unknownUser = 'Uknown username/password';
				// Same as for bad password
			}
    }
		
/*--------------------------------------------------------------------------------------------------*/		
		else if (isset($_SESSION['uid'])) {	// A user is logged in
			$this->maintainSession();
		} 
		
		
			if (isset($_COOKIE['persistent'])&&$_COOKIE['persistent']!='') { //	Do autologin
			$this->autologin();
		}
  }

  /**
	 * Called when successfully logged in and remember me is checked
	 * Creates a new row in the table persistent_login.
	 *
	 * Note, this creates a new persistent login session
	 */
	function persistentLogin () {
		// Create an unique identifier for this login
		$headers = apache_request_headers ();
		$series = md5(session_id());

		// Create a unique identifier that will change for each
		// subsequent auto login
		$token = md5($series.date(DATE_RFC2822).$this->uid.$headers['User-Agent']);

		$encryptedToken = password_hash ($token, PASSWORD_DEFAULT);

		// Store the data in the browser
		setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));

		// Store the data in the database
		$sql = "INSERT INTO persistent_login (uid, series, token)
		VALUES (?, ?, ?)";


		$sth = $this->db->prepare ($sql);
		// Note, the token that changes is stored encrypted
		$sth->execute (array ($this->uid, $series, $encryptedToken));
	}

  /**
	 * Call this method when a user logs out and has persistent login
	 * enabled.
	 * This will remove the information about this persistent login
	 * session both from the user browser and from the database.
	 */
	function removePersistentLogin () {
		$loginData = split(":", $_COOKIE['persistent']);
		$uid = $loginData[0];
		$series = substr($loginData[1], 0, 32);

		$sql = 'DELETE from persistent_login WHERE uid=? and series=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($uid, $series));
		setcookie('persistent');
	}

  /**
   * This function is called if no other login exists but
   * an persistent login cookie was set.
   *
   * Attempts an auto login. If uid and series id exists but
   * the token does not match an intrucion alert will be raised
   * and all existing login information will be removed.
   */
  function autoLogin () {
    $loginData = split(":", $_COOKIE['persistent']);
    if (count($loginData)==0)
      return;
    $uid = $loginData[0];
    $series = substr($loginData[1], 0, 32);
    $token = substr($loginData[1], 32, 32);
    // echo ("{$_COOKIE['persistent']}</br>$uid</br>$series</br>$token</br>");	// Used for debug purposes
    $sql = 'SELECT uid, series, token FROM persistent_login WHERE uid=? AND series=?';
    $sth = $this->db->prepare($sql);
    $sth->execute (array($uid, $series));
    if ($row = $sth->fetch()) {
      if (password_verify($token, $row['token'])) {
        $_SESSION['uid'] = $uid;
        $this->uid = $uid;
        $this->email = $email;
        // Create a unique identifier that will change for each
        // subsequent auto login
        $token = md5($series.date(DATE_RFC2822).$this->uid.$headers['User-Agent']);

        $encryptedToken = password_hash ($token, PASSWORD_DEFAULT);

        // Store new data in the browser
        setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));

        // Update the data in the database
        $sql = "UPDATE persistent_login SET token=? WHERE uid=? AND series=?";

        $sth = $this->db->prepare ($sql);
        // Note, the token that changes is stored encrypted
        $sth->execute (array ($encryptedToken, $this->uid, $series));
        $this->maintainSession ();
      } else { // Userid/series id found but no matching token
        // This suggests a stolen cookie, some one has gained access
        // as the user.
        $sql = "DELETE FROM persistent_login WHERE uid=?";

        $sth = $this->db->prepare ($sql);
        // Note, the token that changes is stored encrypted
        $sth->execute (array ($uid));
        $this->alert = "Possible security breach. All sessions terminated. You should log on and change your password.";
      }
    } // No userid/series identificator found, ignoring the information
  }

  /**
   * This method is called if a $_SESSION['uid'] exists
   * Used to get user information from the database.
   */
  function maintainSession () {
    $sql = "SELECT access_level FROM users WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute (array ($_SESSION['uid']));	// Find user information from the database
    if ($row = $sth->fetch()) {					// User found
      $this->uid = $_SESSION['uid'];			// Store user id in object
      $this->admin = ($row['access_level'] == 'admin'?1:0);
    } else {									// No such user
      unset ($_SESSION['uid']);				// Remove user id from session
    }
  }

  /**
   * This method returns true if a user is logged in, false if no
   * user is logged in.
   *
   * @return boolean value of true if a user is logged in, false if no user is logged in.
   */
  function isLoggedIn() {
    return ($this->uid > -1);	// return true if userid > -1
  }

  function getAlertStatus () {
    return $this->alert;
  }

  /**
	 * Use this function to get the user id for the logged in user.
	 * Returns -1 if no user is logged in, or the id of the logged in user.
	 *
	 * @return long integer with user id, or -1 if no user is logged in.
	 */
	function getUID() {
		return $this->uid;
	}

  /**
  *This method returns the user email for the logged in user
  * @return varchar with email, or -1 if no user is logged in.
  **/
  function getEmail() {
    return $this->email;
  }

  /**
  *This method returns the users access level
  * @return user or admin enum value
  **/
  function getAccessLevel() {
    return ($this->admin);
    //return ($this->access_level == 'admin'?1:0);
  }

	/**
	 * Use this function to get the name of the user.
	 * Returns NULL if no user is logged in, an array with given name and
	 * surename of a user is logged in.
	 *
	 * @return array containing first and last name of user
	 * !!!!Need to save the email in a variable!!!!
*	function getEmail() {
*		return $this->email;
*	}*/

  /**
	 * This method inserts the HTML code for the login form when it is called.
	 * Note that any login attempt detected by the constructor will affect
	 * the outcome of this method. If a successfull login has been performed
	 * this method return an empty string.
	 * If a failed login has been detected an alert box will be shown informing
	 * the user of this fact. The user name will also be filled in with information
	 * from the failed attempt.
	 *
	 * This method also uses the calling script as recipient in the action attribute
	 * of the form.
	 *
	 */
  function insertLoginForm () {
    if ($this->isLoggedIn())	// User is logged in
      return;					// Do not insert anything.
    $email = 'value=""';
    $access_level = 'value=""';
    if (isset($this->unknownUser)) {	// If failed login
      // Set alert and email to be used in the form
      $this->alert = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> '.$this->unknownUser.'</div>';
			if(isset($_POST['login-email']))
      $email = "value='{$_POST['login-email']}' ";
    }
    if (isset($_POST['access_level'])) {
      $access_level = 'value="'.$_POST['access_level'].'"';
    }
    require_once "user.loginform.inc.php";
  }

  function deleteUsr ($id) {
  		$sql = 'DELETE FROM persistantLogin WHERE uid=?';
  		$sth = $this->db->prepare ($sql);
  	    $sth->execute (array ($id));			// Delete from persistant login

  		$sql = 'DELETE FROM users WHERE id=?';
  	    $sth = $this->db->prepare ($sql);
  	    $sth->execute (array ($id));

      return (array ('success'=>'success'));
  }

  /*Skriv beskrivelse av adduser senere!!*/
  function addUser ($uname, $pwd, $access_level) {
		$sql = 'INSERT INTO users (email, password, access_level) VALUES (?, ?, ?)';
	    $sth = $this->db->prepare ($sql);
	    // Add the user to the database
	    $sth->execute (array ($uname, password_hash($pwd, PASSWORD_DEFAULT), $access_level));
	    if ($sth->rowCount()==0) {	// If unable to create user
			  $this->unknownUser = 'email address already registered';
	    	return (array ('error'=>'error', 'description'=>'email address already registered'));
	    }
      if($this->isLoggedIn() == 0){
        $this->uid = $this->db->lastInsertId();
       $this->access_level = $row['access_level'];
       $_SESSION['uid'] = $this->uid;

      }
	     return (array ('success'=>'success'));
	}

}

$user = new User($db);
