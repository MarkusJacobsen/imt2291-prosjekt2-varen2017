<!--
Code for signin/signup form was copied from http://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel
Adapted for this project by Øivind Kolloen, January 2014
-->

<div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-default" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                    </div>

                    <div style="padding-top:30px" class="panel-body" >

						<?php echo $this->getAlertStatus(); ?>

                        <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="email" class="form-control" name="login-email" required <?php echo $email; ?> placeholder="username or email">
                                    </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="login-password" required placeholder="password">
                                    </div>



                            <div class="input-group">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                                    </div>


                                <div style="margin-top:10px; float:right" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <input type="submit" id="btn-login" class="btn btn-primary" value="Login"/>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account!
                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>
                            </form>



                        </div>
                    </div>
        </div>
        <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form" method="post" action="signin.php">

							<?php echo $this->getAlertStatus(); ?>

                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="email" required <?php echo $email; ?> placeholder="Email Address">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" required placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="access_level" class="col-md-3 control-label">Admin? (Debug purpose)</label>
                                    <input name="access_level" value="user" type="hidden">
                                    <input type="checkbox" name="access_level" value="admin">
                                <div class="form-group">
                                    <!-- Button -->
                                  <div style="margin-top:10px; float:right; margin-right:40px" class="form-group">
                                    <div class="col-md-offset-3 col-md-9">
                                        <input type="submit" id="btn-signup" class="btn btn-primary" onclick="myFunction()" value="Sign Up"/>

                                        <script>
                                          function myFunction() {
                                              alert("Vi bruker cookies på vår side.");
                                          }
                                        </script>

                                    </div>
                                  </div>
                                </div>
                            </form>
                         </div>
                    </div>
         </div>
    </div>
