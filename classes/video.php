<?php
require_once('/../include/db.php');
require_once('playlist.php');
if(isset($_GET['id'])&&isset($_GET['add'])){
  $res = $playlist->addEntryToPlaylist($_GET['id'], $_GET['add']);
  if (isset($res['success'])) {?>
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Video lagt til
    </div><?php
  } else {?>
    <div class="alert alert-danger" role="alert">Noe gikk galt</div><?php
  }
  unset($_GET['add']);
}

class Video {
  var $db;
  var $id = -1;
  var $myVideo = false;


  function Video($db) {
    $this->db = $db;
  }

  function time2string($timeline) {
    $times = array();
    $i = 0;
    $periods = array('day' => 86400, 'hour' => 3600, 'minute' => 60, 'second' => 1);

    foreach($periods AS $name => $seconds) {
      $num = floor($timeline / $seconds);
      $timeline -= ($num * $seconds);
      $temp = $num.' '.$name.(($num > 1) ? 's' : '').' ago';
      if($num != 0){
        $times[$i] = $temp;
        $i++;
      }
    }
    if(count($times) === 0) {
      $highest = "Just uploaded";
    } else {
      $highest = $times[0];
    }
    return trim($highest);
  }

  function videoTable($sort, $style) {
        if($style === 0) {?>
          <table id="videot"  class="table table-striped">
          <thread>
            <th></th><th style="width:20px">Tittel</th><th style="width:50px">Beskrivelse</th><th style="width:50px">Opplastet</th>
          </thread>
          <tbody><?php
        $sortingVar = ($sort?'uploadTime DESC':'title ASC');
        $result = $this->db->prepare("SELECT id, title, description, uploadTime FROM `video` ORDER BY $sortingVar");
        $result->execute();
        while($row=$result->fetch(PDO::FETCH_ASSOC)){
          $id = $row['id'];
          $time = $row['uploadTime'];
          $timeFin = $this->time2string(time()-strtotime($time));

          $sql2 = "SELECT thumbnail_filepath FROM videoextra WHERE vid=?";
          $sth2 = $this->db->prepare($sql2);
          $sth2->execute(array($id));
          while($row2=$sth2->fetch(PDO::FETCH_ASSOC)){
            $thumb_src = $row2['thumbnail_filepath'];
          }
          echo '<tr>';
          echo "<td><a href='displayVideo.php?id=$id'><img src='$thumb_src' class='img-thumbnail' alt='thumb' width='200' height='100'></td></a>";
          echo "
          <td>
            <a href='displayVideo.php?id=$id'>{$row['title']}</a>
          </td>
          <td>
            <div class='content hideContent'>{$row['description']}</div>
            <div class='show-more'><a href='#'>Show more</a></div>
          </td>
          <td>$timeFin ago</td>";
          echo '</tr>';
        }

      } else {?>
        <table id="videot"  class="table table-striped">
        <thread>
          <th style="width:200px">Tittel</th>
        </thread>
        <tbody><?php
        $sortingVar = ($sort?'uploadTime DESC':'title ASC');
        $result = $this->db->prepare("SELECT id, title FROM `video` ORDER BY $sortingVar");
        $result->execute();
        while($row=$result->fetch(PDO::FETCH_ASSOC)){
          $id = $row['id'];

				$sql2 = "SELECT thumbnail_filepath FROM videoextra WHERE vid=?";
          $sth2 = $this->db->prepare($sql2);
          $sth2->execute(array($id));
          while($row2=$sth2->fetch(PDO::FETCH_ASSOC)){
            $thumb_src = $row2['thumbnail_filepath'];
          }

          echo '<tr>';
					echo "<td><a href='displayVideo.php?id=$id'><img src='$thumb_src' class='img-thumbnail' alt='thumb' width='120' height='90'></td></a>";
          echo "<td><a href='displayVideo.php?id=$id'>{$row['title']}</a</td>";
          echo '</tr>';
      }
  }
  ?>
  </tbody>
</table>
<?php
}

  function deleteVideo($id) {
    $sql = "DELETE FROM video WHERE id=?";
    $sth = $this->db->prepare ($sql);
    $sth->execute (array ($id));

    $sql2 = "DELETE FROM videoextra WHERE vid =?";
    $sth2 = $this->db->prepare($sql2);
    $sth2->execute(array($id));
    if($sth->rowCount()==0){
      return (array ('error'=>'Something went wrong'));
    }
    return (array ('success'=>'success'));
  }

  function updateVideo($id, $title, $description, $tags) {

		$sqlpre = "select * from `videoextra` where vid='$id'";
		$stmtpre = $this->db->prepare($sqlpre);
		$stmtpre->execute();
		while($row = $stmtpre->fetch(PDO::FETCH_ASSOC)){
			$savePath = $row['thumbnail_filepath'];
		}

    $sql = "DELETE FROM videoextra WHERE vid=?";              //Video tag handeling
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    $sql = "INSERT INTO videoextra (vid, tag, thumbnail_filepath) VALUES (?, ?, ?)";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id, $tags, $savePath));
    $tid = $this->db->lastInsertId();


    $sql = "UPDATE video SET title= ?, description= ? WHERE id = ?";
    $sth = $this->db->prepare ($sql);
    $sth->execute (array($title, $description, $id));
    if($sth->rowCount()==0){
      return (array('error'=>'Something went wrong'));
    }
    return array('success'=>'success');
  }

  function searchVideo($keyword) {
    $sql = "SELECT v.id, v.title, v.description, ve.tid, ve.tag FROM video v
            LEFT JOIN videoextra ve ON v.id = ve.vid
            WHERE v.title LIKE '%$keyword%' OR v.description LIKE '%$keyword%' OR ve.tag LIKE '%$keyword%'";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($keyword));
    while($row=$sth->fetch(PDO::FETCH_ASSOC)){
      $id = $row['id'];
      $title = $row['title'];
      $description = $row['description'];

      $sql2 = "SELECT thumbnail_filepath FROM videoextra WHERE vid=?";
      $sth2 = $this->db->prepare($sql2);
      $sth2->execute(array($id));
      while($row2=$sth2->fetch(PDO::FETCH_ASSOC)){
        $thumb_src = $row2['thumbnail_filepath'];
      }

      $tags = explode(" ", $row['tag']);
      echo '<tr>';
      echo "<td><td><a href='displayVideo.php?id=$id'><img src='$thumb_src' class='img-thumbnail' alt='thumb' width='500' height='300'></td>";
      echo "<td><a href='displayVideo.php?id=$id'>$title</a></td><td width='50%'>$description</td>
            <td width='20%'>";
              if($tags[0] != null){
              foreach($tags as $tag)
                echo "<a href='results.php?keyword=$tag'>#$tag </a>";
              }
      echo '</td>';
      echo '</tr>';
    }

  }

  // Funksjon som viser video og tilhørende subtitles fra databasen på video siden
  function displayVideo($id) {
    $sql ="select * from `video` where id='$id'";

    //$sql = "SELECT id, title, filepath FROM video WHERE id = ?";

		$sql2 ="select * from `subtitles` where id='$id'";
		$stmt = $this->db->prepare($sql2);
		$stmt->execute();
		while($ro = $stmt->fetch(PDO::FETCH_ASSOC)){
				if($ro['language'] == "en"){
					$En = $ro['filepath'];
				}
				if($ro['language'] == "ger"){
					$Ge = $ro['filepath'];
				}
				if($ro['language'] == "esp"){
					$Es = $ro['filepath'];
				}
				if($ro['language'] == "no"){
					$No = $ro['filepath'];
				}
		}


    $sth = $this->db->prepare ($sql);
    $sth->execute();
    while($row = $sth->fetch(PDO::FETCH_ASSOC)){
      $file = $row['filepath'];
      $title = $row['title'];
      $description = $row['description'];
      $filename = $row['filename'];
      $time = strtotime($row['uploadTime']);

    }


    // Henter ut valgt språk på subtitles
	/*
		$sql ="select * from `subtitles` where id='$id'";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		while($ro = $stmt->fetch(PDO::FETCH_ASSOC)){
				if($ro['language'] == "en")
					$En = $ro['filepath'];
				if($ro['language'] == "ger")
					$Ge = $ro['filepath'];
				if($ro['language'] == "esp")
					$Es = $ro['filepath'];
				if($ro['language'] == "no")
					$No = $ro['filepath'];
						}*/

    ?>
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo $title?></title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>

    </head>
    <body>
      <div class="container-fluid">
        <div class="panel-group">
          <div class="col-xs-12 col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title?></h3>
              </div>
              <div class="row" id="video">
                <div class="col-xs-6 col-lg-6">

								<video id="myVideo" width="800" height="450" autoplay controls>
                  <source src="<?php echo"$file" ?>" default>
									<track id="estrack" label="Spanish subtitles" kind="captions" src="<?php echo"$Es" ?>" srclang="sp" ></track>
                  <track id="entrack" label="English subtitles" kind="captions" src="<?php echo"$En" ?>" srclang="en" default ></track>
                  <track id="detrack" label="Germam subtitles" kind="captions" src="<?php echo"$$Ge" ?>" srclang="de" ></track>
									<track id="notrack" label="Norwegian subtitles" kind="captions" src="<?php echo"$No" ?>" srclang="no" /></track>

									</video>
              </div>

                <div class="col-xs-6 col-lg-6">
                 <!-- <button onclick="cuesFromVideoElement();">Get a cue from video element</button> -->
                  <button onclick="cuesFromTrackElement();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/f1.png" /></button>
									 <button id="espButton"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/f2.png" /></button>
									 <button id="deButton"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/f3.png" /></button>
									 <button id="noButton"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/f4.png" /></button>
									 <button id="clearTxt">clear txt test</button>
                  <div style="display:block; overflow:auto; height:500px; width:650px;" id="display"></div>
                </div>
            </div>
						<button onclick="fastBackward();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/fastBack.png" /></button>
						 <button onclick="fastForward();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/fastForward.png" /></button>
						 <button onclick="SlowDownVideo();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/speeddown.png" /></button>
						 <button onclick="SpeedUpVideo();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/speedup.png" /></button>
						 <button onclick="NormalSpeedVideo();"><img src="http://localhost/imt2291-prosjekt2-varen2017/Buttons/normal20.png" /></button>
            <div class="panel-body" id="beskrivelse">
                <h5>

                  <b>Publisert </b> <?php echo date("M j, Y",$time) ?> <hr>
                  <div class="dropdown">
                    <button class="btn btn-md btn-warning dropdown-toggle" type="button" data-toggle="dropdown">
                      <span><i class="glyphicon glyphicon-plus"></i></span>&nbsp; Legg til i spilleliste</button>
                      <ul class="dropdown-menu">
                        <?php
                          $sql = "SELECT title FROM playlist";
                          $sth = $this->db->prepare($sql);
                          $sth->execute();
                          while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                           echo '<li>';
                           echo "<a href='displayVideo.php?id=$id&add={$row['title']}'>{$row['title']}</a>";
                           echo '</li>';
                         }?>
                         <li>
                          <a href="newPlayList.php">Ny spilleliste</a>
                        </li>
                      </ul>
                    </div>
                  <br><br>
                  <?php echo $description;?>
                </h5>

              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
      <script type="text/javascript">


/*
      //  Hvis det skal lastes texttracks fra video fil kan denne funksjonen legges til.
      function cuesFromVideoElement(){
        var oVideo = document.getElementById("myVideo");  // get video object
        var oTrackList = oVideo.textTracks;  // get list of tracks on video object
        for(var i = 0; i < oTrackList.length; i++){
          var oTextTrack = oTrackList[i];      // get first textTrack object (English)
        }
        getCues(oTextTrack);                 // get cues from textTrack object and display
        //getCues2(oTextTrack);
      }
*/
      //  Get a textTrack object through the track element
      function cuesFromTrackElement() {
				document.getElementById("display").innerHTML='';
        var oTrack = document.getElementById("entrack");  // get track object from English track

        var oTextTrack = oTrack.track;                    // get track list
        getCues(oTextTrack);                 // get cues from textTrack object and display
        //getCues2(oTextTrack);
      }
			document.getElementById("noButton").addEventListener("click", getNorwegian, false);
			document.getElementById("espButton").addEventListener("click", getSpanishCues, false);
			document.getElementById("deButton").addEventListener("click", getGerman, false);
			//document.getElementById("deButton").addEventListener("click", getGerman, false);



			document.getElementById("clearTxt").addEventListener("click", function (){
				document.getElementById("display").innerHTML='';

			}, false);

      // Får cue innhold fra textTrack OBJ
      function getCues(oTextTrack){
        var oCueList = oTextTrack.cues  //får en liste av cues som er assosiert med tekstracken
        for (var i = 0; i < oCueList.length; i++) {
          var oMyCue = oCueList[i];       // cue ut listen

          var sText = oMyCue.text;        // får text cue
          var sTime = oMyCue.startTime;   // får start time cue
          var eTime = oMyCue.endTime;     // får end time  cue
          show(sText, sTime,eTime);
        }
      }
			function getGerman(){
				var gTrack = document.getElementById("detrack");
				var geTrack = gTrack.track;
				geTrack.mode = "hidden";
				if( geTrack.cues!=null){
					runDMC(geTrack);
				}
				gTrack.addEventListener("load", function(){
					runDMC(geTrack);
				});
			}

			function getSpanishCues(){
				document.getElementById("display").innerHTML='';
				var sTrack = document.getElementById("estrack");
				var stTrack = sTrack.track;
				stTrack.mode = "hidden";
				if ( stTrack.cues!=null) {
					runDMC(stTrack);
				}
				sTrack.addEventListener("load", function() {
					runDMC(stTrack);
				});
			}
			function getNorwegian(){
				document.getElementById("display").innerHTML='';
				var nTrack = document.getElementById("notrack");
				var noTrack = nTrack.track;
				noTrack.mode = "hidden";
				if(noTrack.cues!=null){
					runDMC(noTrack);
				}
				nTrack.addEventListener("load", function(){
					runDMC(nTrack);
				});
			}
			function runDMC(stTrack){
				document.getElementById("display").innerHTML='';
				var CueList = stTrack.cues;
					for(var i = 0; i < CueList.length; i++){
						var sCue = CueList[i];

						var esTxt = sCue.text;
						var estartTime = sCue.startTime;
						var estopTime = sCue.endTime;
						show(esTxt, estartTime, estopTime);
					}
			}

      //  Display the content in a div element
      function show(sText, sTime,eTime) {
        var count = 0;
        var video = document.getElementById("myVideo");

        var dsp = document.getElementById("display"); // get display area. inn hit det blir formatert ut
        var sstime = sTime;
        //dsp.innerHTML += "<br /> "+ sText + "<span id='tab'>";  // viser kun tekst
        //dsp.innerHTML += "from: "+ sTime + "<span id='tab2'>"; // viser start time
        //dsp.innerHTML += "too: " + eTime; // show end time

        dsp.innerHTML += "<a id='count++' title='test' href='#' onclick='setTime("+sTime+");return false;'>" + sText + " :  " + sTime + " : " + eTime+ "<br /><br />";
        //dsp.innerHTML += "<a href='javascript:setTime(sstime);'" + sText + "'onclick='setTime(sstime);'>" + sTime + eTime + "<br /><br />";
        //dsp.innerHTML += "<a href='#myVideo' onclick='javascript:setTime(sTime)'" + sText + ">" + sText + sTime + eTime +"<br /><br />";
        //dsp.innerHTML += "<a href='javascript:void(video.currentTime = sTime)'" + sText + ">" + sText + sTime + eTime;

      }
			//funksjon som setter videotid lik tiden den mottar
      function setTime(tValue) {

        video.currentTime = tValue;
      }
			//hoppe frem 10sec i filmen
			function fastForward(){
				var Ptime = 10;
				video.currentTime += Ptime;
			}
			//hoppe tilbake 10sec i filmen
			function fastBackward(){
				var Mtime = -10;
				video.currentTime += Mtime;
			}
			//sette playTime minus 25prosent
			function SlowDownVideo(){
				video.playbackRate -= .25;
			}
			//sette playTime pluss 25prosent
			function SpeedUpVideo(){
				video.playbackRate += .25;
			}
			//sette tilbake speed til normal 1 / 100prosent
			function NormalSpeedVideo(){
				video.playbackRate = 1;
			}
					//Listener event. Sjekker at første track er loadet før avspilling begynner så browseren ikke throws an IO exception

      </script>
      <script type="text/javascript">
			//ble lagt ned hit så scriptet ovenfor greide lese av veriden til myVideo
      var video = document.getElementById("myVideo");
      </script>
    </body>
    </html><?php

		}


}


$video = new Video($db);   // The object is to be used for other tasks than displaying a video
