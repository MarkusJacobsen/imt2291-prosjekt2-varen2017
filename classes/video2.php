<?php
session_start();
header("Content-Type: application/json");
require_once '../include/db.php';
require_once '../include/autocreateDatabase.php';
require_once '../classes/user.php';



if(isset($_POST['action']) && !empty($_POST['action'])){
	$action = $_POST['action'];
	if(isset($_POST['id'])){
	$id = $_POST['id'];
	}

	if(isset($_POST['pid'])){
	$pid = $_POST['pid'];
	}
	if(isset($_POST['vid'])){
	$vid = $_POST['vid'];
	}
	if(isset($_POST['userId'])){
	$uid = $_POST['userId'];
	}

	switch($action){
		case 'videoTest' : videoTest($db); break;
		case 'videoplayer' : videoPlayer($db, $id); break;
		case 'myPlaylists' : myPlaylists($db); break;
		case 'addToMyPlaylist': addToMyPlaylist($db, $pid, $vid); break;
		case 'myVideos': myVideos($db, $uid); break;
		case 'videoPlaylistEntries': videoPlaylistEntries($db, $pid); break;
		case 'deleteVideo': deleteVideo($db, $vid); break;

		default: die("Access denied for this function");
	}
}
function videoPlayer($db, $id){
	
	$sql = "select * from `video` where id='$id'";
		/* $sql = "select id, title, description, filename, filepath, idsub, subname, subpath from `video` 
	inner join subtitles_ed on video.id = subtitles_ed.idsub where id='$id'"; 
		$res = $db->query($sql);
	$vid = $res->fetchAll(PDO::FETCH_ASSOC);
	$sql = "select * subtitles_ed where idsub='$id'";
	$res = $db->query($sql);
	$sub = $res->fetchAll(PDO::FETCH_ASSOC);
	$id = array($vid, $sub);
	echo json_encode($)
	*/ 
	$res = $db->query($sql);
	echo json_encode($res->fetchAll(PDO::FETCH_ASSOC));
}

  function videoTest($db) {
		
		$sql = "select id, title, description, uploadTime, thumbnail_filepath from `video`
		inner join videoextra on video.id = videoextra.vid";
		$res = $db->query($sql);
		echo json_encode($res->fetchAll(PDO::FETCH_ASSOC));
	}
	
	function myPlaylists($db){
		$myUID->getUID();
		$sql = "select title from playlist where owner='$myUID'";
		$res = $db->query($sql);
		echo json_encode($res->fetchAll(PDO::FETCH_ASSOC));
	}
	
	
	
	  function time2string($timeline) {
    $times = array();
    $i = 0;
    $periods = array('day' => 86400, 'hour' => 3600, 'minute' => 60, 'second' => 1);

    foreach($periods AS $name => $seconds) {
      $num = floor($timeline / $seconds);
      $timeline -= ($num * $seconds);
      $temp = $num.' '.$name.(($num > 1) ? 's' : '').' ago';
      if($num != 0){
        $times[$i] = $temp;
        $i++;
      }
    }
    if(count($times) === 0) {
      $highest = "Just uploaded";
    } else {
      $highest = $times[0];
    }
    return trim($highest);

  }

	function myVideos($db, $uid){
			$sql = "select id, title, description, uploader, uploadTime, thumbnail_filepath from `video`
			inner join videoextra on video.id = videoextra.vid WHERE uploader = $uid";
			$res = $db->query($sql);
			echo json_encode($res->fetchAll(PDO::FETCH_ASSOC));
	}

	function videoPlaylistEntries($db, $pid) {
		$sql = "select id, title, description, uploadTime, thumbnail_filepath from `video`
		inner join videoextra on video.id = videoextra.vid
		inner join playlistentry on video.id = playlistentry.vid WHERE playlistentry.pid = '$pid'";
		$res = $db->query($sql);
		echo json_encode($res->fetchAll(PDO::FETCH_ASSOC));
	}

	function deleteVideo($db, $vid) {
		//Delete video from server
		$sql = 'DELETE FROM video WHERE id=?';
		$sth = $db->prepare ($sql);
		$sth->execute (array ($vid));

		//Delete video entry in every playlist which the video existed
	//  $sql = 'DELETE FROM playlistentry WHERE vid=?';
	//  $sth = $db->prepare ($sql);
	// $sth->execute (array ($vid));
	}

