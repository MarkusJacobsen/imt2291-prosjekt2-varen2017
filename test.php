<?php	
header("Content-Type: application/json");
require_once ('include/db.php');
session_start();


//login bruker.
if(isset($_POST['login-email'])&&isset($_POST['login-password'])){
	$error = array('status'=>'something went wrong with processing your loginData');
		$mail = trim($_POST['login-email']);
		$pwd = trim($_POST['login-password']);

		$login = false;
		
		$sql = "SELECT id, password, email, access_level FROM users WHERE email=?";
		$sth = $db->prepare($sql);
		$sth->execute (array ($mail));	// Get user info from the database
		if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// If user exists
			if (password_verify ($pwd, $row['password'])) {

				$_SESSION['uid'] = $row['id'];
				$uid = $_SESSION['uid'];	
				$access_level = $row['access_level'];
				$user = $row['email'];
				$login = true;
				$id = array($uid, $user, $access_level, $login);
				$series = md5($mail.(time ()));
				$token = md5($uid.(time ()));
				setcookie("persistantLogin", $uid.";".$series.";".$token, time()+60*60*3);
				$sql = "insert into persistent_login (uid, series, token) VALUES (?, ?, ?)";
				$stmt = $db->prepare($sql);
				$stmt->execute (array ($uid, $series, $token));
				
				$sql = "select email, access_level from users where id=?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($_SESSION['uid']));
				if($row = $stmt->fetch()){
					$uid = $_SESSION['uid'];
					$myID = $uid;
					$access_level = $row['access_level'];
					$user = $row['email'];
					$login = true;
					$id = array($uid, $user, $access_level, $login);
					echo json_encode($id);

					//echo json_encode($id);
				}
			}else{
				echo json_encode($error);
			}
		}else{
			echo json_encode($error);
		}
}
	
//registrere ny bruker.	
if(isset($_POST['email'])&&isset($_POST['password'])){
	$mail = $_POST['email'];
	$pwd = $_POST['password'];
	$access_level = 'user';	
	if(isset($_POST['access_level']))
		$access_level = 'admin';
	
	$sql = 'insert into users (email, password, access_level) VALUES (?, ?, ?)';
	$stmt = $db->prepare($sql);
	$row = $stmt->execute(array ($mail, password_hash($pwd, PASSWORD_DEFAULT), $access_level));
		if($stmt->rowCount()==0){
			echo json_encode(array('error'=>'error', 'description'=>'email adress already registered'));
		}else{
			$uid = $db->lastInsertId();
			$_SESSION['uid'] = $uid;
			//$id = array($uid, $mail, $access_level);
			//echo json_encode($id); 
			$id = array('Status'=>'Success');
			echo json_encode($id);
		}
}

if(isset($_POST['action']) && !empty($_POST['action'])){
	$action = $_POST['action'];

	switch($action){
		case 'persistantLogin' : persistantLogin($db); break;
		case 'logout' 				 : logout(); break;
		default: die("Access denied for this function");
	}
}
//sjekk om cookie og session_uid er satt.	
function persistantLogin($db){

	if (isset($_COOKIE['persistantLogin'])) {
		$error = array('status'=>'something went wrong with processing your loginData');
		$persistantData = explode(";", $_COOKIE['persistantLogin']);
		$sql = "SELECT uid, series, token FROM persistent_login WHERE uid=? AND series=?";
		$sth = $db->prepare ($sql);
		$sth->execute (array ($persistantData[0], $persistantData[1]));
			
		if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {            
			if ($row['token']==$persistantData[2]) {
				$_SESSION['uid'] = $row['uid'];
				$uid = $persistantData[0];
				$series = $persistantData[1];
				$token = md5($uid.(time()));
				setcookie("persistantLogin", $uid.";".$series.";".$token, time()+60*60*3);
				$sql = "UPDATE persistent_login set uid=? where token=? and series=?";
				$sth1 = $db->prepare($sql);
				$sth1->execute (array ($uid, $series, $token));
      }
			if(isset($_SESSION['uid'])){
				$sql = "select email, access_level from users where id=?";
				$stmt = $db->prepare($sql);
				$stmt->execute(array($_SESSION['uid']));
				if($row = $stmt->fetch()){
					$uid = $_SESSION['uid'];
					$myID = $uid;
					$access_level = $row['access_level'];
					$user = $row['email'];
					$login = true;
					$id = array($uid, $user, $access_level, $login);
					echo json_encode($id);
				}else{
					unset ($_SESSION['uid']);
				}
			}
		}else{
			echo json_encode($error);
		}
	}
}

function logout(){
	
	unset($_SESSION['uid']);
	setcookie("persistentLogin", "", time());
	$uid = -1;
	$login = false;
	$id = array($uid, $login);
	echo json_encode($id);
        
}
		


	
	
	
	

?>