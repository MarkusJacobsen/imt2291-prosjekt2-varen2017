<?php
//session_start();
require_once('include\db.php');
//require_once('classes\user.php');
require_once('test.php');
header("Content-Type: application/json");

set_time_limit(0);
ini_set('upload_max_filesize', '500M');
ini_set('post_max_size', '500M');
ini_set('max_input_time', 4000); // Play with the values
ini_set('max_execution_time', 4000); // Play with the values


//require_once('include/heading.php'); //Får 3 error ved å include denne. Derfor kommenterer jeg den ut. nå fungerer det igjen.
//require_once('include/playlistStream.php');


if(isset($_POST['title'])){
                                   
    $target_dir = "uploads/";
    $target_dir_thumb = 'uploads/thumb/';
    $target_file = $target_dir . basename($_FILES["file"]["name"]); // Henter inn fila som formatet skal sjekkes mot

    $allowedExt = array("mp4, wmv, mkv"); // Definerer lovlige filformater for feilmelding
		
		$user_id = -1;
		if(isset($_POST['id'])){
			$user_id =  $_POST['id'];
		}

    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION); // Henter format fra filepath

    if((($imageFileType == "mp4") || ($imageFileType == "wmv") || ($imageFileType == "mkv"))) { // Sjekker om filepath består av lovlige filformater


        $AllTags = $_POST['tag'];


        $file = $_FILES['file'];
        $name = preg_replace('/\s+/', '_', $file['name']);    //bytter ut alle "dårlige" tegn med understrek

        //$target_dir_thumb = 'uploads/thumb/';
        //$target_dir = '/xampp/htdocs/imt2291-prosjekt1-youtube2.0/uploads/';

        if(empty($errors)==true){
                        
            if(is_dir($target_dir)==false){
                mkdir("$target_dir", 755);     // Create directory if it does not exist
            }
            if(is_dir($target_dir_thumb)==false){
                mkdir("$target_dir_thumb", 755);     // Create directory if it does not exist
            }

            $path = $target_dir . basename($name);
            if(move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
            } else {
                echo "Something went wrong";
            }
            $url ="http://localhost/imt2291-prosjekt2-varen2017/uploads/$name";

      /* for "video til thumbnail"  skal funksjonere må det installeres ffmpeg på maskin/server. Dette kan gjøres ved eks powershell.
      (dette krever chocolatey: https://chocolatey.org/ ).
      Etter installert choco. Åpne powershell på nytt. tast inn -> "executionPolicy remoteSigned" så -> "choco install ffmpeg" deretter
      trykk 'y' ved bekreftelse. Da vi det bli lastet ned og installert riktig versjon fra repository.
      I linux er kommandoen 'sudo apt-get install ffmpeg'
      etter det skal $ffmpeg settes lik bin/ffmpeg (full path slik som kan sees under)
      Kan bruke dobbel '\\' på bakover-slash så php leser pathway riktig.
      også mulig å søke etter installasjon-filen på google. Lett å finne!
      */


            $ffmpeg ="C:\\ProgramData\\Chocolatey\\lib\\ffmpeg\\tools\\ffmpeg-3.2.2-win64-static\\bin\\ffmpeg.exe";
            //hva thumbnail skal hete
            $imageFile ='thumb_' . preg_replace('/\s+/', '_', $name) . '.jpg';

            //I denne pathwayen er vi allerede inne i mappe repository'et til youtube2.0
            //lagere fil med filnavn inn i uploads/ opprettet ovenfor
            $tmbPath = getcwd() . '\\uploads\\thumb\\' . $imageFile;
            $size = "336x188";                          //bestem størrelse på filen
            $getFromSecond = 2;                         //fra hvilken tid i filmen thumbnail skal hente bilde
            //$cmd = "ffmpeg -i $videoFile -an -ss $getFromSecond -s $size $imageFile";


            $sql = "INSERT INTO video(title, description, uploader, filename, filepath) VALUES (?, ?, ?, ?, ?)";
            $sth = $db->prepare($sql);
            $sth->execute(array($_POST['title'], $_POST['description'], $user_id, $name, $url));
            $vid = $db->lastInsertId();

            $sql = "INSERT INTO videoextra(vid, tag) VALUES (?, ?)";
            $sth = $db->prepare($sql);
            $sth->execute(array($vid, $AllTags));
            $id = $db->lastInsertId();

            //her er allerede videoen lagt inn i mappen. setter lik $url path istedenfor $_FILES['file']['tmp_name']
            $videoFile = getcwd() . '\\' . $path;
            /* ffmpeg syntax:
            -i = input file
            -deinterlace = deinterlace pictures
            -an = disable audio recording
            -ss = start time in the video (seconds)
            -t = duration of the recording (seconds)
            -r = set frame rate
            -y = overwrite existing file
            -s = resolution size
            -f = force format
            */
            //kommando linjen vi skal kjøre
            $cmd = "$ffmpeg -i $videoFile -ss 00:00:02 -vframes 1 $tmbPath";
            if(!shell_exec($cmd)){              //shell execute
                $url = "http://localhost/imt2291-prosjekt2-varen2017/uploads/thumb/$imageFile";
                if(file_exists($tmbPath)){
                    $sql = "UPDATE videoextra SET thumbnail_filepath=? WHERE vid=?";
                    $sth = $db->prepare($sql);
                    $sth->execute(array($url, $vid));
                } else {
                    $imageFile = "undefined.png";
                    $url = "http://localhost/imt2291-prosjekt2-varen2017/uploads/thumb/$imageFile";
                    $sql = "UPDATE videoextra SET thumbnail_filepath=? WHERE vid=?";
                    $sth = $db->prepare($sql);
                    $sth->execute(array($url, $vid));
                }
            } else {
                echo "thumbnail error";
            }
        }
        //echo "<script type='text/javascript'>alert('submitted successfully!')</script>";       
    //} else {        
      //  echo "<script type='text/javascript'>alert('failed!')</script>";
    }
}
?>
