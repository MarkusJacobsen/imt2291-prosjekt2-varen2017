<?php

AutoDB();
function AutoDB (){
	$host="localhost";

	$root="root";
	$root_password="";

	$user='admin';
	$pass='';
	$db="www_prosjekt1";

    try {
        $dbh = new PDO("mysql:host=$host", $root, $root_password);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->exec("CREATE DATABASE IF NOT EXISTS `$db`;
                CREATE USER '$user'@'localhost' IDENTIFIED BY '$pass';
                GRANT ALL ON `$db`.* TO '$user'@'localhost';
                FLUSH PRIVILEGES;")
        or die(print_r($dbh->errorInfo(), true));

    } catch (PDOException $e) {
        die("DB ERROR: ". $e->getMessage());
    }


	try{

/*--------------------------------------------------------------------------------OK*/
	$sql = "use `$db`";
	$dbh->exec($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `users`(
	`id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` char(255) NOT NULL,
  `access_level` enum('user','admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
	SET time_zone = '+01:00';

		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8mb4 */;

	ALTER TABLE `users`
		ADD PRIMARY KEY (`id`),
		ADD UNIQUE KEY `email` (`email`);


	ALTER TABLE `users`
		MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
	";
	$dbh->exec($sql);
/*--------------------------------------------------------------------------------*/




/*--------------------------------------------------------------------------------OK*/
	$sql2 = "use `$db`";
	$dbh->exec($sql2);
	$sql2 = "CREATE TABLE IF NOT EXISTS `playlist` (
  `owner` varchar(255) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `playlist`
  ADD PRIMARY KEY (`pid`);

	ALTER TABLE `playlist`
  MODIFY `pid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
	";
	$dbh->exec($sql2);
/*--------------------------------------------------------------------------------*/




/*--------------------------------------------------------------------------------OK*/
	$sql3 = "use `$db`";
	$dbh->exec($sql3);
	$sql3 = "CREATE TABLE IF NOT EXISTS `playlistentry` (
  `pid` bigint(20) NOT NULL,
  `vid` int(11) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	ALTER TABLE `playlistentry`
  ADD PRIMARY KEY (`pid`,`vid`);
	";
	$dbh->exec($sql3);
/*--------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------OK*/
$sql4 = "use `$db`";
$dbh->exec($sql4);
$sql4 = "CREATE TABLE IF NOT EXISTS `show_user` (
  `id` int(20) NOT NULL,
  `videoid` int(20) NOT NULL,
  `email` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `show_user`
  ADD PRIMARY KEY (`id`);

";

$dbh->exec($sql4);
/*----------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------OK*/
	$sql5 = "use `$db`";
	$dbh->exec($sql5);
	$sql5 = "CREATE TABLE IF NOT EXISTS `subtitles` (
  `id` int(11) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `language` enum('en','ger','esp','no') NOT NULL,
  `filepath` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `subtitles`
  ADD PRIMARY KEY (`id`, `language`);

	ALTER TABLE `subtitles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

	";
	$dbh->exec($sql5);
/*--------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------OK*/
	$sql6 = "use `$db`";
	$dbh->exec($sql6);
	$sql6 = "CREATE TABLE IF NOT EXISTS `persistent_login` (
	`uid` bigint(20) NOT NULL,
  `series` char(32) NOT NULL,
  `token` char(255) NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
	SET time_zone = '+01:00';
		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8mb4 */;

	ALTER TABLE `persistent_login`
		ADD PRIMARY KEY (`uid`,`series`);


	";
	$dbh->exec($sql6);
/*--------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------OK*/
	$sql7 = "use `$db`";
	$dbh->exec($sql7);
	$sql7 ="CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `uploader` int(11) NOT NULL,
  `uploadTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `filename` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
	SET time_zone = '+01:00';

		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8mb4 */;

	ALTER TABLE `video`
		ADD PRIMARY KEY (`id`);

		ALTER TABLE `video`
		MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; AUTO_INCREMENT=0;


	";
	$dbh->exec($sql7);
/*--------------------------------------------------------------------------------*/

  $sql8 = "use `$db`";
	$dbh->exec($sql8);
	$sql8 ="CREATE TABLE IF NOT EXISTS `videoextra` (
  `tid` int(11) NOT NULL,
  `vid` int(11) NOT NULL,
  `tag` varchar(25) NOT NULL,
	`thumbnail_filepath` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `videoextra`
  ADD PRIMARY KEY (`tid`,`vid`,`tag`);

	ALTER TABLE `videoextra`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;


";
	$dbh->exec($sql8);
/*--------------------------------------------------------------------------------*/

	}catch(PDOException $ex){
		die("TABLE ERROR: ". $ex->getMessage());
	}
}
?>
