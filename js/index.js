var uid = null;
var user = null;
var accesslvl = null;
var signedin = null;
var uidtemp = null;


$( document ).ready(function(){
	document.title = "Forelesninger NTNU";
$('#signerUT').hide();
$('#spille_liste').hide();
$('#mylist').hide();
$('#seenby').hide();
$('#newPlaylist').hide();
$('#lastopp').hide();
$('#adminstatus').hide();
$('#MineVideo').hide();

$.ajax({
	url: 'test.php',
	type: "POST",
	async: false,
	data: {action: 'persistantLogin'},
	success: function(response){
		console.log(response);
			uid = response[0];
			user = response[1];
			accesslvl = response[2];
			signedin = response[3];

			if(signedin == true ){
				$('#signerIN').hide();
				$('#signerUT').show();
				$('#mylist').show();
				$('#newPlaylist').show();
			}
			if(signedin == true && accesslvl == 'admin'){
				$('#lastopp').show();
				$('#adminstatus').show();
				$('#MineVideo').show();
				$('#seenby').show();
			}

	},
	error: function(error){
		console.log('-NOT!!!-successfull', error);
	},
	complete: function(xhr, status){
		console.log('the request was successfull');
	}
});

	$(window).on("load", function(){
		var a = 'html/videoStream.html';
		callVideoTable(a);
	});
});


$(document).ready(function(){
document.title = "Forelesninger NTNU";
getPlaylist();
	$('a').click(function(e){
		e.preventDefault();
		var a = $(this).attr('name');

		switch(a){
			case "displayVideo": a = 'html/videoStream.html';
			callVideoTable(a); break;

			case "upload": a = "html/uploadVideo.html";
			callUpload(a); break;

			case "admin" : a = 'html/admin.html';
			callAdmin(a); break;

			case "signin" : a = 'html/signinForm.html';
			callSignin(a); break;

			case "newPlaylist": a ="html/newPlaylist.html";
			callNewPLaylist(a); break;

			case "myVideos": a ="html/myVideos.html";
			callMyVideos(a); break;

			case "seenby": a = "html/seenBy.html";
			callSeenBy(a); break;

			case "signutherifra" : callogout();

			default: a = 'html/videoStream.html';
			callVideoTable(a);
			getPlaylist();

		}

	});
});

////////////////////////////
// DIVERSE SIGNIN/SIGNOUT //
////////////////////////////

function callSignin(confirm){
	$.ajax({
		url: confirm,
		type: "POST",
		success: function(sig){
			console.log('signin html was loaded', sig);
			$('#mainContent').html(sig);
			$('#loginform').submit(function(event){
				event.preventDefault();
				var formData = $('#loginform').serialize();
				$.ajax({
					url: 'test.php',
					type: "POST",
					data: formData,
					mimeType: "multipart/form-data",
					cache: false,
					dataType: "json",
					// processData: false,
					success: function(response){
						//alert(response[0]);
						//alert(response[1]);
						uid = response[0];
						user = response[1];
						accesslvl = response[2];
						signedin = response[3];

						if(signedin == true ){
							$('#signerIN').hide();
							$('#signerUT').show();
							$('#mylist').show();
							$('#newPlaylist').show();

						}
						if(signedin == true && accesslvl == 'admin'){
							$('#lastopp').show();
							$('#adminstatus').show();
							$('#MineVideo').show();
						}
						//alert(response);
						window.location="index.html";
						console.log('User: Status:', response);
					},
					error: function(error){
						console.log('signin didnt load', error);
					},
					complete: function(xhr, status){
						console.log('the request is complete');
					}
				});
			});
			$('#signupform').submit(function(event){
				event.preventDefault();
				var formData = $('#signupform').serialize();
				$.ajax({
					url: 'test.php',
					type: "POST",
					data: formData,
					mimType: "multipart/form-data",
					cashe: false,
					processData: false,
					success: function(response){
						console.log('User creation was:', response);
						$('#loginbox, #signupbox').toggle();
					},
					error: function(error){
						console.log('User was -NOT!!!- registred successfully', error);
					},
					complete: function(xhr, status){
						console.log('the request was successfull');
					}
				});
			});
		$('#mainContent a').click(function(evt){
				evt.preventDefault();
				$('#loginbox, #signupbox').toggle();

			});
		},
		error: function(error){
			console.log('signin didnt load', error);
		},
		complete: function(xhr, status){
		console.log('the request is complete');
		}
	});
}

function callogout(){
	$.ajax({
		url: 'test.php',
		type: "POST",
		data: {action: 'logout'},
		success: function(response){
			$('#signerUT').hide();
			$('#signerIN').show();
			$('#newPlaylist').hide();
			$('#mylist').hide();

			$('#lastopp').hide();
			$('#adminstatus').hide();
			$('#MineVideo').hide();
			$('#seenby').hide();
		},
		error: function(error){
			console.log('signout didnt load', error);
		},
		complete: function(xhr, status){
		console.log('the request is complete');
		}
	});
}

////////////////////////////////
// END DIVERSE SIGNIN/SIGNOUT //
////////////////////////////////

function callVideoTable(pageRefInput){

    $.ajax({
        url: pageRefInput,
        type: "POST",

        success: function( data ) {
            document.title = "Forelesninger NTNU";
            console.log('the page was loaded');
            $('#sidebar-wrapper').show();
            $('#mainContent').html(data);
            $.ajax({
                url: "classes/video2.php",
                data: {action: "videoTest" },
                type: "POST",
                success: function( response ){
                    console.log('videotable list is loaded', response);
                    for(var i=0; i<response.length; i++){
                        var pic = $('<tr><td><a href="#" id="videoFile'+response[i].id+'"><img src="'+response[i].thumbnail_filepath+'"class="imt-thumbnail" alt="thumb" width="200" height="100"></td></a><td><a href="#" id="videoFile'+response[i].id+'"><h5>'+response[i].title+'</h5></td><td><h6>'+response[i].description+'</h6></td><td>'+response[i].uploadTime+'</td></tr></a>');
                        $('#videot > tbody:last-child').append(pic);
                    }
                },
                error: function( error ) {
                console.log('the page was NOT loaded', error);
                },
                complete: function( xhr, status ){
                console.log("the request is complete");
                }
            });
        }
    });
}

$(document).on("click", "a[id^='videoFile']", function(event){
    event.preventDefault();
    var vid = (this).id.replace("videoFile", "");;
    var test = (this).id.split('e');
		var test2 = test[test.length-1];
		console.log(test2);
    $('#sidebar-wrapper').hide();
    callFuc(test2); //Send med ny vid
    logUser(uid, vid, user);//Samme her
});
function callFuc(ID){

	var id_nr = ID;
	$.ajax({
		url: "include/videoplayer.php",
		type: "POST",
		success: function(response) {
			console.log('the page was loaded');
			$('#mainContent').html(response);
			$.ajax({
				url: "classes/video2.php",
				data: { action: "videoplayer", id: id_nr },
				type: "POST",
				success: function( response ){
				console.log('the page was loaded');

				$("#myVideo").html('<source id="videoID'+id_nr+'" src="'+response[0].filepath+'" type="video/mp4"></source>' );
				}
			});
			$.ajax({
			 url: "playlist.php",
			 type: "POST",
			 dataType: "html",
			 data: { action: 'listPlaylists', type: 'dropdownPlaylist', userId: uid }
		 })
		 .done(function(data) {
			 console.log('Playlist content loaded into dropdown menu');
			 $('#playlistsGoesHere').replaceWith(data);
		 })
		 .fail(function(error){
			 console.log('Could not add load playlist in dropdown menu', error);
		 })
		 .always(function(xhr, status){
			 console.log('Playlist load request is complete');
		 });
		}
	});
}
function callUpload(myUpload){
	$.ajax({
		url: myUpload,
		type: "POST",
		success: function(up){
			console.log('upload page is loaded and ready', up);
		document.title = "Ny opplastning";
			$('#mainContent').html(up);
			$('#myNewFile').submit(function(event){
				event.preventDefault();
				var formData = new FormData($(this)[0]);
				formData.append('id', uid);
				$.ajax({
					url: 'uploadVideoBackend.php',
					type: "POST",
					data: formData,
					async: false,
					cache: false,
					contentType: false,
					//enctype: 'multipart/form-data',
					processData: false,
					success: function(response){
						//alert(response);

					}
				});
				//return false;
				window.location="index.html";
			});
		},
		error: function(error){
			console.log('upload page didnt load', error);
		},
		complete: function(xhr, status){
			console.log('the request is complete');
		}
	});

}

function callMyVideos(pageRef){
	$.ajax({
		url: pageRef,
		method: "POST",
		dataType: "html"
	})
	.done(function (data){
		document.title = 'Mine videoer';
		console.log("My videos loaded");
		$('#mainContent').html(data);
		$('#editBoxVideo').hide();
		var vid_id;
		$.ajax({
			url: "classes/video2.php",
			data: {action: "myVideos", userId: uid },
			type: "POST"
		})
			.done(function( response ){
				console.log('My videos entries loaded');
				for(var i=0; i<response.length; i++){
				//	vid_id = response[i].id;
				//	var vidSelector = '#' +response[i].id;
					var pic = $('<tr><td><a href="#" id="videoFilee'+response[i].id+'"><img src="'+response[i].thumbnail_filepath+'"class="imt-thumbnail" alt="thumb" width="200" height="100"></td></a><td><a href="#" id="'+response[i].id+'"><h5>'+response[i].title+'</h5></td><td><h6>'+response[i].description+'</h6></td><td>'+response[i].uploadTime+'</td></a><td><a id=\'deleteVideo\' ><span class=\'glyphicon glyphicon-remove\' aria-hidden=\'true\'></span></a></td><td><a id=\'redigerVideo\' ><span class=\'glyphicon glyphicon-wrench\' aria-hidden=\'true\'></span></a></td></tr>');

					$('#myVideoTable > tbody:last-child').append(pic);
					$(document).on('click', '#deleteVideo', function(deleteVideo){
						deleteVideo.preventDefault();
						var vid_id = (this).parentNode.previousElementSibling.previousElementSibling.previousElementSibling.firstChild.id
						$.ajax({
							url: "classes/video2.php",
							method: "POST",
							data: {action: "deleteVideo", vid: vid_id}
						})
						.done(function(){
							alert("Video slettet");
							console.log("Video deleted from server");
							$('.displayVideo').trigger();
						})
						.fail(function(error){
							console.log('Could not delete video', error);
						})
						.always(function(xhr, status){
							console.log('the request is complete');
						});
					});
				}
			})
			.fail(function(error){
				console.log('My video entries didnt load', error);
			})
			.always(function(xhr, status){
				console.log('the request is complete');
			});
	})
	.fail(function(error){
		console.log('My videos page didnt load', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}
$(document).on("click", "a[id^='videoFilee']", function(event){
    event.preventDefault();
    //var vid = (this).id.replace("videoFile", "");;
    var test = (this).id.split('e');
		var test2 = test[test.length-1];
		console.log(test2);
    $('#sidebar-wrapper').hide();
    callFuc(test2); //Send med ny vid
    //logUser(uid, vid, user);//Samme her
});




////////////////////////////////
// FUNCTIONS RELATED TO ADMIN //
////////////////////////////////
function callAdmin(command){
	document.title = "Administrasjon";
	$.ajax({
		url: command,
		type: "POST"
	})
	.done(function(data) {
		console.log('admin page is loaded and ready');
		$('#mainContent').html(data);
		$("#myNewUser").submit(function(event){
			event.preventDefault();
			var formData = new FormData($(this)[0]);//$(this).serialize();
			formData.append('action', 'addUser');
			$.ajax({
				url:"admin.php",
				type:"POST",
				data:formData,
				async: false,
				cache: false,
				contentType: false,
				enctype: "multipart/form-data",
				processData: false
			})
			.done(function(data) {
				console.log('New user added', data);
				getUserInformation();
			})
			.fail(function(error){
				console.log('Could not add new user', error);
			})
			.always(function(xhr, status){
				console.log('User add request is complete');
			});
		})
		getUserInformation();
	})
	.fail(function(error){
		console.log('admin page didnt load', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}

function getUserInformation(){
	$.ajax({
		url: "admin.php",
		type: "POST",
		dataType: "html",
		data: {action: 'listUsers'}
	})
	.done(function(data) {
		console.log('User content loaded');
		$('#user-information').html(data);
	})
	.fail(function(error){
		console.log('Could not load user information', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}

$(document).on("click", "a[id='deleteUser']", function(event){
	event.preventDefault();
	var emailToDelete = (this).parentElement.parentElement.firstElementChild.innerHTML;
	if(confirmationBox()) {
		$.ajax({
			url: "admin.php",
			method: "POST",
			data: { email: emailToDelete }
		})
		.done(function() {
			getUserInformation();
		})
		.fail(function(error){
			console.log('Could not delete user', error);
		})
		.always(function(xhr, status){
			console.log('the request is complete');
		});
	}
});

//https://www.w3schools.com/howto/howto_js_sort_table.asp
function sortTable(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("brukerTabel");
	switching = true;
	//Set the sorting direction to ascending:
	dir = "asc";
	/*Make a loop that will continue until
	no switching has been done:*/
	while (switching) {
		//start by saying: no switching is done:
		switching = false;
		rows = table.getElementsByTagName("TR");
		/*Loop through all table rows (except the
		first, which contains table headers):*/
		for (i = 1; i < (rows.length - 1); i++) {
			//start by saying there should be no switching:
			shouldSwitch = false;
			/*Get the two elements you want to compare,
			one from current row and one from the next:*/
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			/*check if the two rows should switch place,
			based on the direction, asc or desc:*/
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					//if so, mark as a switch and break the loop:
					shouldSwitch= true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					//if so, mark as a switch and break the loop:
					shouldSwitch= true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			/*If a switch has been marked, make the switch
			and mark that a switch has been done:*/
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			//Each time a switch is done, increase this count by 1:
			switchcount ++;
		} else {
			/*If no switching has been done AND the direction is "asc",
			set the direction to "desc" and run the while loop again.*/
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

///////////////
// END ADMIN //
///////////////
////////////////////////////////////
// FUNCTIONS RELATED TO PLAYLIST //
///////////////////////////////////
function callNewPLaylist(pageRef){
	$.ajax({		//Calls some html
		url: pageRef,
		type: "POST"
	})
	.done(function(data){
		$('#mainContent').html(data);
		document.title = "Ny spilleliste";

		$("#myNewPlaylist").submit(function(event){
		event.preventDefault();
		var formData = new FormData($(this)[0]);//$(this).serialize();
		formData.append('action', 'addPlaylist');
		formData.append('userId', uid);
		$.ajax({
				url:"playlist.php",
				type:"POST",
				data:formData,
				async: false,
				cache: false,
				contentType: false,
				enctype: "multipart/form-data",
				processData: false
			})
			.done(function(data) {
				console.log('New playlist added', data);
				alert('New playlist added');
				callVideoTable('html/videoStream.html');
			})
			.fail(function(error){
				console.log('Could not add new playlist', error);
			})
			.always(function(xhr, status){
				console.log('Playlist add request is complete');
				getPlaylist();
			});
	})
})
	.fail(function(error){
		console.log('Could not load new playlist form', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}

function getPlaylist(){
	$.ajax({
		url: "playlist.php",
		type: "POST",
		dataType: "html",
		data: {action: 'listPlaylists', userId: uid}
	})
	.done(function(data) {
		console.log('Playlist content loaded');
		$('#playlistListing > li:nth-child(3)').html(data);
	})
	.fail(function(error){
		console.log('Could not load playlists', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}

$(document).on("click", "a[id^='playlist']", function(e){
	e.preventDefault();
	console.log((this).innerHTML);
	var PID = (this).id.replace("playlist", "");
	var vTitle = (this).innerHTML;
	 $.ajax({
	 	// Send med titel eller id til en html side som lister ut videoer i denne spillelisten
		url: "playlist.php",
		data: {action: 'userPlaylist', title: vTitle},
		method: "POST"
	})
	.done(function(data){
		console.log("Spesific playlist content loaded");
		document.title = "Playlist - "+vTitle;
		$('#mainContent').html(data);
		$('#redigerSpilleliste').click(function(){
			// $(this).parentNode.attr('disabled', 'disabled');
			editPlaylist(PID);
		});
		var vid_id;
		$.ajax({
			url: "classes/video2.php",
			data: {action: "videoPlaylistEntries", pid: PID },
			type: "POST"
		})
			.done(function( response ){
				console.log('Playlist videotable list is loaded', response);
				for(var i=0; i<response.length; i++){
					vid_id = response[i].id;
					vid_id_id = '#'+vid_id;
					var pic = $('<tr><td><a href="#" id="videoFileee'+response[i].id+'"><img src="'+response[i].thumbnail_filepath+'"class="imt-thumbnail" alt="thumb" width="200" height="100"></td></a><td><a href="#" id="'+response[i].id+'"><h5>'+response[i].title+'</h5></td><td><h6>'+response[i].description+'</h6></td><td>'+response[i].uploadTime+'</td></a><td><a id=\'deletePlaylistVideo\' ><span class=\'glyphicon glyphicon-remove\' aria-hidden=\'true\'></span></a></td></tr>');

					$('#playlistTable > tbody:last-child').append(pic);
				}
			})
			.fail(function( error ) {
			console.log('the page was NOT loaded', error);
		})
			.always(function( xhr, status ){
			console.log("the request is complete");
		});
	})
	.fail(function(error){
		console.log('Could not load Spesific playlists', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
});
$(document).on("click", "a[id^='videoFileee']", function(event){
    event.preventDefault();
    //var vid = (this).id.replace("videoFile", "");;
    var test = (this).id.split('e');
		var test2 = test[test.length-1];
		console.log(test2);
    $('#sidebar-wrapper').hide();
    callFuc(test2); //Send med ny vid
    //logUser(uid, vid, user);//Samme her
});

$(document).on("click", "a[id^='dropdownPl']", function(e){
	e.preventDefault();
	console.log();
	var PID = (this).id.replace("dropdownPl", "");
	var VID = document.getElementById('myVideo').childNodes[0].id.replace("videoID", "");
	 $.ajax({
	 	// Send med titel eller id til en html side som lister ut videoer i denne spillelisten
		url: "playlist.php",
		data: { action: "addToMyPlaylist", pid: PID, vid: VID},
		method: "POST"
	})
	.done(function(data){
		alert("Video lagt til i spillelisten");
	})
	.fail(function(error){
		console.log('Video entry could not be added to playlist', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
});

function editPlaylist(pid){
	$.ajax({
		url: "html/redigerSpilleliste.html",
		method: "POST",
		dataType: "html"
	})
	.done(function(data){
		$('#playlistContainer').append(data);
		$("#myUpdatedPlaylist").submit(function(event){
			event.preventDefault();
			var formData = new FormData($(this)[0]);//$(this).serialize();
			formData.append('action','updatePlaylist');
			formData.append("pid", pid);
			$.ajax({
				url:"playlist.php",
				type:"POST",
				data:formData,
				async: false,
				cache: false,
				contentType: false,
				enctype: "multipart/form-data",
				processData: false
			})
			.done(function(data){
				getPlaylist();
				// Fungerer ikke som det skal
				// var reloadPid = '#playlist'+pid;
				// $(reloadPid).trigger("click");
			})
			.fail(function(error){
				console.log('Could not update playlist', error);
			})
			.always(function(xhr, status){
				console.log('Playlist update request is complete');
			});
		})
	})
	.fail(function(error){
		console.log('Could not load edit box', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
}

$(document).on("click", "#deletePlaylistVideo", function(e){
	var PID = (this).parentNode.parentNode.parentNode.parentNode.parentNode.previousElementSibling.childNodes[1].id;
	var VID = (this).parentElement.parentElement.childNodes[0].childNodes[0].id.replace("videoFileee", "");
	$.ajax({
		url: "playlist.php",
		method: "POST",
		data: {action: 'deletePlaylistVideo', pid: PID, vid: VID}
	})
	.done(function(){
		alert("Video slettet fra spilleliste");
		var url = '#playlist'+PID;
		$(url).trigger("click");
	})
	.fail(function(error){
		console.log('Could not delete video from playlist', error);
	})
	.always(function(xhr, status){
		console.log('the request is complete');
	});
});



//////////////////
// END PLAYLIST //
//////////////////



///////////////////
// DIVERSE TOOLS //
///////////////////
function confirmationBox() {
	var con = window.confirm('Sikker på at du vil slette denne brukeren?');
	return con;
}

// Legger inn hvem som har sett på en video i DB.
function logUser(id, vid, userEmail) {

	$.ajax({
		url: "logUser.php",
		async: false,
		data: {
			user_log_id: id,
			user_log_vid: vid,
			user_log_email: userEmail
		},
		type: "POST",

		// Får tilbake informasjon om hva som er blitt lagt til i DB.
		success: function(data) {
			console.log("Lagt til brukerID:", id, "videoID:", vid, "Email:", userEmail);
		},
		error: function(error) {
			// Feilmelding dersom noe gikk galt ved innsetting av info.
			console.log("ERROR: User not found/missing arguments", error);
		},
		// Når ajax er ferdig kommer en melding om det.
		complete : function(xhr, status) {
			console.log("The request is complete");
		}
	});
}

// Lister ut alle videoer som er sett og hvem de/den er sett av.
function callSeenBy(seen) {

	$.ajax({
		url: seen,
		type: "POST",

		// Viser tabellen for utskrift.
		success: function(data) {
			document.title = "Statistikk";
			$('#mainContent').html(data);
			console.log("Hentet data");
			var vidTitle;
			var userEmail;

			// Nytt kall til DB for ønsket data som skal i tabellen.
			$.ajax({
				url: "showUserSeen.php",
				type: "POST",
				dataType: "JSON",

				// Henter info om hvilken video som er sett og av hvem den er sett.
				success: function(response) {
					console.table(response);

					for (var i = 0; i < response.length; i++) {

						vidTitle = response[i].title;
						userEmail = response[i].email;

						// Lister ut innhentet info i rader som legges etter hverandre.
						var listOut = $('<tr><td>'+vidTitle+'</td><td>'+userEmail+'</td></tr>');
						$('#videot > tbody:first-child').append(listOut);
					}
				},
				// Dersom feil kommer en melding om det.
				error: function(error) {
					console.log(error);
				},
				// Når ajax er ferdig kommer en melding om det.
				complete: function(xhr, status) {
					console.log("The request is complete");
				}
			});
		},
		// Dersom feil kommer en melding om det.
		error: function(error) {
			console.log(error);
		},
		// Når ajax er ferdig kommer en melding om det.
		complete: function(xhr, status) {
			console.log("The request is complete");
		}
	});
}

$('#results').submit(function(event){
	event.preventDefault();
	var formData = new FormData($(this)[0]);
	$.ajax({
		url: 'html/results.html',
		method: "POST",
		dataType: "html"
	})
	.done(function(data){
		console.log("Search page loaded");
		$('#mainContent').html(data);
		$.ajax({
				url: 'results.php',
				type: "POST",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
			})
			.done(function(response){
				document.title = "Søk";
				console.log("Search came back successful", response);
				$('myVideoTable').html(response);
				for(var i=0; i<response.length; i++){
						var pic = $('<tr><td><a href="#" id="videoFile'+response[i].id+'"><img src="'+response[i].thumbnail_filepath+'"class="imt-thumbnail" alt="thumb" width="200" height="100"></td></a><td><a href="#" id="videoFile'+response[i].id+'"><h5>'+response[i].title+'</h5></td><td><h6>'+response[i].description+'</h6></td><td>'+response[i].tag+'</td></tr></a>');
						$('#myVideoTable > tbody:last-child').append(pic);
				}
			})
			.fail(function(){

			})
			.always(function(){

			});
	})
	.fail(function(error) {
		console.log(error);
	})
	.always(function(xhr, status) {
		console.log("The request is complete");
	});

});
