<?php

session_start();
require_once('include\db.php');
require_once('classes\user.php');


// Legger inn data som skal logges for brukere som har sett på video.
if(isset($_POST['user_log_id']) && isset($_POST['user_log_vid']) && isset($_POST['user_log_email'])) {
  $sql = "INSERT INTO show_user(id, videoid, email) VALUES (?, ?, ?)";
  $sth = $db->prepare($sql);
  $sth->execute(array($_POST['user_log_id'], $_POST['user_log_vid'], $_POST['user_log_email']));
}
